var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuvw",
  1: "bdfhlrstu",
  2: "bcmt",
  3: "abdfhlmprstu",
  4: "bcdefghilnoprstuv",
  5: "ci",
  6: "hp",
  7: "blmnsvw",
  8: "cdeilprsuvw",
  9: "lt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties",
  9: "Pages"
};


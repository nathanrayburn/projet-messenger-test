var searchData=
[
  ['frmmain_23',['frmMain',['../class_chat_messenger_g_u_i_1_1frm_main.html',1,'ChatMessengerGUI.frmMain'],['../class_chat_messenger_g_u_i_1_1frm_main.html#a5aa4b8dea1cf5c8e661660b581f8a816',1,'ChatMessengerGUI.frmMain.frmMain()'],['../class_chat_messenger_g_u_i_1_1frm_main.html#ac572d306b2d4a15e7e130daf5170f358',1,'ChatMessengerGUI.frmMain.frmMain(User user)']]],
  ['frmmain_2ecs_24',['frmMain.cs',['../frm_main_8cs.html',1,'']]],
  ['frmmain_2edesigner_2ecs_25',['frmMain.Designer.cs',['../frm_main_8_designer_8cs.html',1,'']]],
  ['frmsign_26',['frmSign',['../class_chat_messenger_g_u_i_1_1frm_sign.html',1,'ChatMessengerGUI.frmSign'],['../class_chat_messenger_g_u_i_1_1frm_sign.html#a28ee7c81daf444d9b74fe3c425fc16f9',1,'ChatMessengerGUI.frmSign.frmSign()']]],
  ['frmsign_2ecs_27',['frmSign.cs',['../frm_sign_8cs.html',1,'']]],
  ['frmsign_2edesigner_2ecs_28',['frmSign.Designer.cs',['../frm_sign_8_designer_8cs.html',1,'']]],
  ['fromjson_29',['FromJson',['../class_model_1_1_settings.html#a10ca16c3ccd8d27f561f40d6ae3a4d68',1,'Model::Settings']]],
  ['fromjson_5fbasiccase_5fsuccess_30',['FromJson_BasicCase_Success',['../class_test_chat_messenger_1_1_test_settings.html#aae17c27942e0dfa08729ded951effb23',1,'TestChatMessenger::TestSettings']]]
];

var searchData=
[
  ['validateandreplacepassword_239',['ValidateAndReplacePassword',['../class_b_crypt_1_1_net_1_1_b_crypt.html#a3d99a5f7fff2ce36d07f5f3406a8ca42',1,'BCrypt.Net.BCrypt.ValidateAndReplacePassword(string currentKey, string currentHash, string newKey, int workFactor=DefaultRounds, bool forceWorkFactor=false)'],['../class_b_crypt_1_1_net_1_1_b_crypt.html#a2212a1dfea9077e5e6870f29f7f79998',1,'BCrypt.Net.BCrypt.ValidateAndReplacePassword(string currentKey, string currentHash, bool currentKeyEnhancedEntropy, HashType oldHashType, string newKey, bool newKeyEnhancedEntropy=false, HashType newHashType=DefaultEnhancedHashType, int workFactor=DefaultRounds, bool forceWorkFactor=false)']]],
  ['verify_240',['Verify',['../class_b_crypt_1_1_net_1_1_b_crypt.html#ab4cbfbcbe562d669e0563eaea0916078',1,'BCrypt::Net::BCrypt']]]
];

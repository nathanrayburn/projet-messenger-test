﻿using MetroFramework.Controls;
using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Net.Mail;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using Model;
using MySql.Data.MySqlClient;

namespace ChatMessengerGUI
{
    /// <summary>
    /// This user control is designed for the login page of our application and is inherited from an external windows form theme "Metro"
    /// </summary>
    public partial class ucLogin : MetroUserControl
    {
        #region private attributes
        private int indexLastChange;
        private StringBuilder password = new StringBuilder();
        private TimerCallback timerCallback;
        private System.Threading.Timer timer;
        private string tempEmail = ""; //this email is used when the user has went through the validation process and clicks on "connect" on the valided registration userControl
        #endregion private attributes

        #region constructor
        /// <summary>
        /// This constructor is used initially when the application is launched at first and when navigating
        /// this initializes the components and instances a call back for timer
        /// </summary>
        public ucLogin()
        {
            InitializeComponent();
            timerCallback = new TimerCallback(Do);
        }

        /// <summary>
        /// This is used when the user has went through the validation process and clicks on "connect" on the valided registration userControl
        /// </summary>
        /// <param name="tempEmail"></param>
        public ucLogin(string tempEmail)
        {
            InitializeComponent();

            timerCallback = new TimerCallback(Do);
            chkRememberMe.Checked = false;
            this.tempEmail = tempEmail;

            if (tempEmail != "")
            {
                txtEmail.Text = tempEmail;
            }
        }
        #endregion constructor

        #region private methods
        private void linkToRegister_Click(object sender, System.EventArgs e)
        {
            frmSign.Instance.Content.Controls.Clear();
            frmSign.Instance.Content.Controls.Add(new ucRegister());
        }

        private void cmdLogin_Click(object sender, System.EventArgs e)
        {
            bool noEmail = string.IsNullOrWhiteSpace(txtEmail.Text);
            bool noPassword = string.IsNullOrWhiteSpace(password.ToString());
            bool emailNotVaild = IsValid(txtEmail.Text);

            if (noEmail || noPassword)
            {
                MessageBox.Show(@"Il faut remplir tous les champs !", @"Erreur dans le formulaire", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if (noEmail) txtEmail.WithError = true; 
  
                if (noPassword) txtPassword.WithError = true; 
                
                return;
            }
            if (!emailNotVaild) {
                MessageBox.Show(@" Veuillez entre un e-mail valable !", @"Erreur dans le formulaire", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtEmail.WithError = true;
                return;
            }


            User user = new User(txtEmail.Text,password.ToString());
            DBUserManager dbUserManager = new DBUserManager(Settings.Instance.DbConnectionString,user);
            try
            {
                dbUserManager.Login();

                if (chkRememberMe.Checked)
                {
                    Settings.Instance.UserEmail = txtEmail.Text;
                }

                frmSign.Instance.Hide();
                frmMain mainApplication = new frmMain(user);
                mainApplication.ShowDialog();
                frmSign.Instance.Controls.Clear();

            }
            catch (MySqlException exception)
            {
                MessageBox.Show("Erreur de connexion avec le serveur, ceci peut-être un problème technique.", @"Error | Chat Messenger");
                
            }
            catch (LoginFailException)
            {
                MessageBox.Show("L'authentification n'a pas fonctionné", @"Erreur | Chat Messenger");
            }
        }
        /// <summary>
        /// This function is designed to test if the email is valid or not
        /// </summary>
        /// <param name="emailaddress"></param>
        /// <returns></returns>
        private bool IsValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// This funciton is designed to mask every character after one second
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPassword_TextChanged(object sender, System.EventArgs e)
        {

            StringBuilder input = new StringBuilder(txtPassword.Text); //set the input of the textbox to a string builder
            var cursorIndex = txtPassword.SelectionStart;   // Get the starting position of the cursor

            for (int i = 0; i < txtPassword.Text.Count(); i++)   //Go through the textbox's characters
            {
                if (input[i] != '●')
                {
                    if (i >= password.Length)
                    {
                        password.Append(input[i]);
                    }
                    else
                    {
                        if (input.Length > password.Length)
                        {
                            password.Insert(i, input[i]); //insert if a letter has been typed in the middle of the text into the clear stringBuilder
                        }
                        else
                        {
                            password[i] = txtPassword.Text.ElementAt(i); //replace the letter
                        }
                    }

                    if (indexLastChange == i && input.Length > 0) //replace the clear character in the textbox
                    {
                        input[i] = '●';
                    }
                    indexLastChange = i; //Update the last index of a character that has changed
                }
            }

            int dif = password.Length - input.Length;
            if (dif > 0)
            {
                password.Remove(cursorIndex, dif); //remove a certain number of numbers in the clear password if have removed one or more characters

            }

            txtPassword.Text = input.ToString(); //update the textbox
            Console.WriteLine("password " + password); //show the password in console

            if (timer == null)
            {

                timer = new System.Threading.Timer(timerCallback, null, 1000,0); //create  a new timer if the old one has been destroyed
            }
            else {
              timer.Change(1000, 0);  //reset the timer to 1000ms
            }
            int num = this.txtPassword.Text.Count();
            if (num > 1)
            {
                StringBuilder s = new StringBuilder(this.txtPassword.Text);
                s[num - 2] = '●'; //replace the before to last character to a star
                this.txtPassword.Text = s.ToString();
                this.txtPassword.SelectionStart = num;
            }

            this.txtPassword.SelectionStart = (cursorIndex > 0) ? cursorIndex : 0;   //third operator , test if the cursor is negative , it will go back to 0

            Console.WriteLine("Last index change :" + (indexLastChange)); //show the index in the console

        }

        private void Do(object state)
        {


            int num = this.txtPassword.Text.Count();
            if (num > 0)
            {
                StringBuilder s = new StringBuilder(this.txtPassword.Text);

                s[num - 1] = '●';                                   //hide the last character after one second
                this.Invoke(new Action(() =>
                {
                    var cursorIndex = txtPassword.SelectionStart;   //get the cursor index
                    this.txtPassword.Text = s.ToString();           // update the textbox
                    this.txtPassword.SelectionStart = cursorIndex;  //reset the cursor to the current cursor
                    timer.Dispose();                                //destroy the timer
                    timer = null;
                }));
            }
        }

        private void chkRememberMe_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRememberMe.Checked == false)
            {
                Settings.Instance.UserEmail = "";
            
            }
        }

        private void ucLogin_Load(object sender, EventArgs e)
        {
            txtEmail.Text = Settings.Instance.UserEmail;
            if (txtEmail.Text != "")
            {
                chkRememberMe.Checked = true;
            }

        }
        #endregion private methods
    }
}

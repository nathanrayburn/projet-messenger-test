﻿namespace ChatMessengerGUI
{
    partial class ucRegister
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRegister = new MetroFramework.Controls.MetroLabel();
            this.lblEmail = new MetroFramework.Controls.MetroLabel();
            this.lblPassword = new MetroFramework.Controls.MetroLabel();
            this.lblPasswordVerification = new MetroFramework.Controls.MetroLabel();
            this.txtEmail = new MetroFramework.Controls.MetroTextBox();
            this.txtPassword = new MetroFramework.Controls.MetroTextBox();
            this.txtPasswordVerification = new MetroFramework.Controls.MetroTextBox();
            this.cmdRegister = new System.Windows.Forms.Button();
            this.linkToLogin = new MetroFramework.Controls.MetroLink();
            this.lblStrength = new MetroFramework.Controls.MetroLabel();
            this.lblStrengths = new MetroFramework.Controls.MetroLabel();
            this.progressionBarStrength = new MetroFramework.Controls.MetroProgressBar();
            this.SuspendLayout();
            // 
            // lblRegister
            // 
            this.lblRegister.AutoSize = true;
            this.lblRegister.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblRegister.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblRegister.Location = new System.Drawing.Point(3, 12);
            this.lblRegister.Name = "lblRegister";
            this.lblRegister.Size = new System.Drawing.Size(103, 25);
            this.lblRegister.TabIndex = 1;
            this.lblRegister.Text = "Inscription";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(51, 56);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(115, 19);
            this.lblEmail.TabIndex = 2;
            this.lblEmail.Text = "Entrez votre email";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(78, 85);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(88, 19);
            this.lblPassword.TabIndex = 3;
            this.lblPassword.Text = "Mot de passe";
            // 
            // lblPasswordVerification
            // 
            this.lblPasswordVerification.AutoSize = true;
            this.lblPasswordVerification.Location = new System.Drawing.Point(3, 114);
            this.lblPasswordVerification.Name = "lblPasswordVerification";
            this.lblPasswordVerification.Size = new System.Drawing.Size(163, 19);
            this.lblPasswordVerification.TabIndex = 4;
            this.lblPasswordVerification.Text = "Mot de passe (vérification)";
            // 
            // txtEmail
            // 
            // 
            // 
            // 
            this.txtEmail.CustomButton.Image = null;
            this.txtEmail.CustomButton.Location = new System.Drawing.Point(194, 1);
            this.txtEmail.CustomButton.Name = "";
            this.txtEmail.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtEmail.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtEmail.CustomButton.TabIndex = 1;
            this.txtEmail.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtEmail.CustomButton.UseSelectable = true;
            this.txtEmail.CustomButton.Visible = false;
            this.txtEmail.Lines = new string[0];
            this.txtEmail.Location = new System.Drawing.Point(172, 56);
            this.txtEmail.MaxLength = 32767;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.PasswordChar = '\0';
            this.txtEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEmail.SelectedText = "";
            this.txtEmail.SelectionLength = 0;
            this.txtEmail.SelectionStart = 0;
            this.txtEmail.ShortcutsEnabled = true;
            this.txtEmail.Size = new System.Drawing.Size(216, 23);
            this.txtEmail.TabIndex = 5;
            this.txtEmail.UseSelectable = true;
            this.txtEmail.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtEmail.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtPassword
            // 
            // 
            // 
            // 
            this.txtPassword.CustomButton.Image = null;
            this.txtPassword.CustomButton.Location = new System.Drawing.Point(194, 1);
            this.txtPassword.CustomButton.Name = "";
            this.txtPassword.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPassword.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPassword.CustomButton.TabIndex = 1;
            this.txtPassword.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPassword.CustomButton.UseSelectable = true;
            this.txtPassword.CustomButton.Visible = false;
            this.txtPassword.Lines = new string[0];
            this.txtPassword.Location = new System.Drawing.Point(172, 85);
            this.txtPassword.MaxLength = 32767;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '\0';
            this.txtPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPassword.SelectedText = "";
            this.txtPassword.SelectionLength = 0;
            this.txtPassword.SelectionStart = 0;
            this.txtPassword.ShortcutsEnabled = true;
            this.txtPassword.Size = new System.Drawing.Size(216, 23);
            this.txtPassword.TabIndex = 6;
            this.txtPassword.UseSelectable = true;
            this.txtPassword.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPassword.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtPassword.TextChanged += new System.EventHandler(this.txtPassword_TextChanged);
            // 
            // txtPasswordVerification
            // 
            // 
            // 
            // 
            this.txtPasswordVerification.CustomButton.Image = null;
            this.txtPasswordVerification.CustomButton.Location = new System.Drawing.Point(194, 1);
            this.txtPasswordVerification.CustomButton.Name = "";
            this.txtPasswordVerification.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPasswordVerification.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPasswordVerification.CustomButton.TabIndex = 1;
            this.txtPasswordVerification.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPasswordVerification.CustomButton.UseSelectable = true;
            this.txtPasswordVerification.CustomButton.Visible = false;
            this.txtPasswordVerification.Lines = new string[0];
            this.txtPasswordVerification.Location = new System.Drawing.Point(172, 114);
            this.txtPasswordVerification.MaxLength = 32767;
            this.txtPasswordVerification.Name = "txtPasswordVerification";
            this.txtPasswordVerification.PasswordChar = '\0';
            this.txtPasswordVerification.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPasswordVerification.SelectedText = "";
            this.txtPasswordVerification.SelectionLength = 0;
            this.txtPasswordVerification.SelectionStart = 0;
            this.txtPasswordVerification.ShortcutsEnabled = true;
            this.txtPasswordVerification.Size = new System.Drawing.Size(216, 23);
            this.txtPasswordVerification.TabIndex = 7;
            this.txtPasswordVerification.UseSelectable = true;
            this.txtPasswordVerification.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPasswordVerification.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtPasswordVerification.TextChanged += new System.EventHandler(this.txtPasswordVerification_TextChanged);
            // 
            // cmdRegister
            // 
            this.cmdRegister.BackColor = System.Drawing.Color.MediumTurquoise;
            this.cmdRegister.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdRegister.FlatAppearance.BorderSize = 0;
            this.cmdRegister.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSeaGreen;
            this.cmdRegister.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRegister.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRegister.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.cmdRegister.Location = new System.Drawing.Point(3, 171);
            this.cmdRegister.Name = "cmdRegister";
            this.cmdRegister.Size = new System.Drawing.Size(385, 23);
            this.cmdRegister.TabIndex = 8;
            this.cmdRegister.Text = "Valider votre inscription";
            this.cmdRegister.UseVisualStyleBackColor = false;
            this.cmdRegister.Click += new System.EventHandler(this.cmdRegister_Click);
            // 
            // linkToLogin
            // 
            this.linkToLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.linkToLogin.DisplayFocus = true;
            this.linkToLogin.Location = new System.Drawing.Point(3, 200);
            this.linkToLogin.Name = "linkToLogin";
            this.linkToLogin.Size = new System.Drawing.Size(385, 23);
            this.linkToLogin.TabIndex = 9;
            this.linkToLogin.Text = "Me connecter";
            this.linkToLogin.UseSelectable = true;
            this.linkToLogin.Click += new System.EventHandler(this.linkToLogin_Click);
            // 
            // lblStrength
            // 
            this.lblStrength.AutoSize = true;
            this.lblStrength.Location = new System.Drawing.Point(172, 140);
            this.lblStrength.Name = "lblStrength";
            this.lblStrength.Size = new System.Drawing.Size(41, 19);
            this.lblStrength.TabIndex = 15;
            this.lblStrength.Text = "None";
            // 
            // lblStrengths
            // 
            this.lblStrengths.AutoSize = true;
            this.lblStrengths.Location = new System.Drawing.Point(20, 140);
            this.lblStrengths.Name = "lblStrengths";
            this.lblStrengths.Size = new System.Drawing.Size(146, 19);
            this.lblStrengths.TabIndex = 14;
            this.lblStrengths.Text = "Your password strength";
            // 
            // progressionBarStrength
            // 
            this.progressionBarStrength.Location = new System.Drawing.Point(242, 143);
            this.progressionBarStrength.Name = "progressionBarStrength";
            this.progressionBarStrength.Size = new System.Drawing.Size(146, 16);
            this.progressionBarStrength.TabIndex = 16;
            // 
            // ucRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.progressionBarStrength);
            this.Controls.Add(this.lblStrength);
            this.Controls.Add(this.lblStrengths);
            this.Controls.Add(this.cmdRegister);
            this.Controls.Add(this.linkToLogin);
            this.Controls.Add(this.txtPasswordVerification);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.lblPasswordVerification);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblRegister);
            this.Name = "ucRegister";
            this.Size = new System.Drawing.Size(400, 227);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblRegister;
        private MetroFramework.Controls.MetroLabel lblEmail;
        private MetroFramework.Controls.MetroLabel lblPassword;
        private MetroFramework.Controls.MetroLabel lblPasswordVerification;
        private MetroFramework.Controls.MetroTextBox txtEmail;
        private MetroFramework.Controls.MetroTextBox txtPassword;
        private MetroFramework.Controls.MetroTextBox txtPasswordVerification;
        private System.Windows.Forms.Button cmdRegister;
        private MetroFramework.Controls.MetroLink linkToLogin;
        private MetroFramework.Controls.MetroLabel lblStrength;
        private MetroFramework.Controls.MetroLabel lblStrengths;
        private MetroFramework.Controls.MetroProgressBar progressionBarStrength;
    }
}

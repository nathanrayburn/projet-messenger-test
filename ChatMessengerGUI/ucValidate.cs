﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;

namespace ChatMessengerGUI
{

    /// <summary>
    /// This user control is designed to notify the user when the registration has been accepted
    /// </summary>
    public partial class ucValidate : MetroUserControl
    {
        #region private attributes
        private string userEmail;
        #endregion private attributes

        #region constructor
        /// <summary>
        /// This constructor is used initially when the application is launched at first and when navigating
        /// this initializes the components
        /// </summary>
        public ucValidate()
        {
            InitializeComponent();
        }
        /// <summary>
        /// This constructor is used when the user has done a valid registration
        /// Initializes the label and retrieves the user's email
        /// </summary>
        /// <param name="userEmail"></param>
        public ucValidate(string userEmail)
        {
            InitializeComponent();
            this.userEmail = userEmail;
            lblEmail.Text = userEmail + ",vous êtes bien inscrit-e";
        }
        #endregion constructor

        #region private attributes
        /// <summary>
        /// Login Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdLogin_Click(object sender, EventArgs e)
        {
            frmSign.Instance.Content.Controls.Clear();
            frmSign.Instance.Content.Controls.Add(new ucLogin(userEmail));
        }
        #endregion private attributes
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;
namespace ChatMessengerGUI
{
    public partial class ucMain : UserControl
    {
        #region private attributes
        private static frmSign instance;
        private User user;
        #endregion private attributes
        public ucMain()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        public ucMain(User user)
        {
            this.user = user;
            InitializeComponent();
            lblEmail.Text = this.user.Email;
        }

        private void picBoxAddContact_Click(object sender, EventArgs e)
        {
            frmAddContacts contacts = new frmAddContacts(user);
            contacts.ShowDialog();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using MetroFramework.Components;
using MetroFramework.Controls;
using MetroFramework.Forms;
using Model;
using System.IO;

namespace ChatMessengerGUI
{
    /// <summary>
    /// This form is designed for the home page and inherited from an external library theme for windows form
    /// </summary>
    public partial class frmMain : MetroForm
    {
        #region private attributes
        private static frmMain instance;
        private User user;
        #endregion private attributes

        #region constructor

        /// <summary>
        /// This constructor :
        /// - Creates an instance of the home page form
        /// - Initializes the components of the form
        /// - Loads the theme for windows form from an external library
        /// </summary>
        public frmMain()
        {
            instance = this;
            InitializeComponent();
            InitializeTheme();

        }

        public frmMain(User user)
        {
            this.user = user;
            instance = this;
            InitializeComponent();
            InitializeTheme();
            pnlMain.Controls.Add(new ucMain(user));
        }
        #endregion constructor

        #region accessor
        /// <summary>
        /// This accessor is designed to be the only instance of frmMain
        /// </summary>
        public static frmMain Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new frmMain();
                }

                return instance;
            }
        }

        /// <summary>
        /// Computer generated from the Metro library
        /// Use the panel for the user controls such as login and register
        /// </summary>
        public MetroPanel Content
        {
            get => pnlMain;
            set => pnlMain = value;
        }

        #endregion accessor

        #region private methods
        private void InitializeTheme()
        {
            // Initialize Metro Modern Ui
            MetroStyleManager metroStyleManager = new MetroStyleManager();
            metroStyleManager.Style = MetroColorStyle.Red;
            metroStyleManager.Theme = MetroThemeStyle.Light;
            StyleManager = metroStyleManager;
        }
        /// <summary>
        /// This function is designed to ask the user if he wants to close the application and also save the settings of the application to the json file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                dynamic result = MessageBox.Show("Voulez-vous quitter ?", "Chat Messeneger", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    Settings.Instance.WindowsX = Location.X;
                    Settings.Instance.WindowsY = Location.Y;
                    File.WriteAllText("settings.json",Settings.Instance.ToJson());
                    Application.Exit();
                }

                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }
        #endregion private methods
    }
}

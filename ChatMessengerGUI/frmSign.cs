﻿using MetroFramework;
using MetroFramework.Components;
using MetroFramework.Controls;
using MetroFramework.Forms;
using System.Windows.Forms;
using Model;
using System.IO;

namespace ChatMessengerGUI
{
    public partial class frmSign : MetroForm
    {
        #region private attributes
        private static frmSign instance;
        #endregion private attributes

        #region constructor
        /// <summary>
        /// This constructor :
        /// - Initializes the form's components,
        /// - Instances the settings that are read form the local json file,
        /// - Loads the settings from the local json file,
        /// - Loads in first the login page
        /// </summary>
        public frmSign()
        {
            string settingsInJson = File.ReadAllText("settings.json");
            Settings.Instance.FromJson(settingsInJson);

            instance = this;
            InitializeComponent();
            InitializeTheme();
            pnlSign.Controls.Add(new ucLogin());
          
            if (Settings.Instance.WindowsX != -1 && Settings.Instance.WindowsY != -1)
            {
                StartPosition = FormStartPosition.Manual;
                Left = Settings.Instance.WindowsX;
                Top = Settings.Instance.WindowsY;
            }
        }
        #endregion constructor

        #region accessor
        /// <summary>
        /// This accessor is designed to be the only instance of frmSign
        /// </summary>
        public static frmSign Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new frmSign();
                }

                return instance;
            }
        }
        /// <summary>
        /// Computer generated from the Metro library
        /// Use the panel for the user controls such as login and register
        /// </summary>
        public MetroPanel Content
        {
            get => pnlSign;
            set => pnlSign = value;
        }
        #endregion accessor

        #region private methods
        private void InitializeTheme()
        {
            // Initialize Metro Modern Ui
            MetroStyleManager metroStyleManager = new MetroStyleManager();
            metroStyleManager.Style = MetroColorStyle.Red;
            metroStyleManager.Theme = MetroThemeStyle.Light;
            StyleManager = metroStyleManager;
        }

        /// <summary>
        /// Saves the windows location when closing the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSign_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                dynamic result = MessageBox.Show("Voulez-vous quitter ?", "Chat Messeneger", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {

                    
                    Settings.Instance.WindowsX = frmSign.instance.Location.X;
                    Settings.Instance.WindowsY = frmSign.instance.Location.Y;
                    File.WriteAllText("settings.json",Settings.Instance.ToJson());
                    Application.Exit();
                }

                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }
        #endregion private methods
    }
}
﻿namespace ChatMessengerGUI
{
    partial class ucValidate
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEmail = new MetroFramework.Controls.MetroLabel();
            this.lblValidate = new MetroFramework.Controls.MetroLabel();
            this.cmdLogin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblEmail
            // 
            this.lblEmail.Location = new System.Drawing.Point(3, 60);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(385, 19);
            this.lblEmail.TabIndex = 3;
            this.lblEmail.Text = ", vous êtes bien inscrit-e";
            this.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblValidate
            // 
            this.lblValidate.AutoSize = true;
            this.lblValidate.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblValidate.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblValidate.Location = new System.Drawing.Point(105, 20);
            this.lblValidate.Name = "lblValidate";
            this.lblValidate.Size = new System.Drawing.Size(198, 25);
            this.lblValidate.TabIndex = 4;
            this.lblValidate.Text = "Enregistrement validé";
            // 
            // cmdLogin
            // 
            this.cmdLogin.BackColor = System.Drawing.Color.LightCoral;
            this.cmdLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdLogin.FlatAppearance.BorderSize = 0;
            this.cmdLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.cmdLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdLogin.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdLogin.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.cmdLogin.Location = new System.Drawing.Point(3, 101);
            this.cmdLogin.Name = "cmdLogin";
            this.cmdLogin.Size = new System.Drawing.Size(394, 23);
            this.cmdLogin.TabIndex = 5;
            this.cmdLogin.Text = "Me connecter";
            this.cmdLogin.UseVisualStyleBackColor = false;
            this.cmdLogin.Click += new System.EventHandler(this.cmdLogin_Click);
            // 
            // ucValidate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cmdLogin);
            this.Controls.Add(this.lblValidate);
            this.Controls.Add(this.lblEmail);
            this.Name = "ucValidate";
            this.Size = new System.Drawing.Size(400, 127);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblEmail;
        private MetroFramework.Controls.MetroLabel lblValidate;
        private System.Windows.Forms.Button cmdLogin;
    }
}

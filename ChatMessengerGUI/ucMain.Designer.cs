﻿namespace ChatMessengerGUI
{
    partial class ucMain
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEmail = new MetroFramework.Controls.MetroLabel();
            this.cmdConversation = new MetroFramework.Controls.MetroButton();
            this.lblContacts = new MetroFramework.Controls.MetroLabel();
            this.picBoxAddContact = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxAddContact)).BeginInit();
            this.SuspendLayout();
            // 
            // lblEmail
            // 
            this.lblEmail.Location = new System.Drawing.Point(429, 36);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(276, 23);
            this.lblEmail.TabIndex = 0;
            this.lblEmail.Text = "nathan.rayburn@cpnv.ch";
            this.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmdConversation
            // 
            this.cmdConversation.Location = new System.Drawing.Point(565, 264);
            this.cmdConversation.Name = "cmdConversation";
            this.cmdConversation.Size = new System.Drawing.Size(185, 40);
            this.cmdConversation.TabIndex = 1;
            this.cmdConversation.Text = "Create a conversation";
            this.cmdConversation.UseSelectable = true;
            // 
            // lblContacts
            // 
            this.lblContacts.Location = new System.Drawing.Point(474, 264);
            this.lblContacts.Name = "lblContacts";
            this.lblContacts.Size = new System.Drawing.Size(85, 40);
            this.lblContacts.TabIndex = 3;
            this.lblContacts.Text = "Add Contact";
            this.lblContacts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picBoxAddContact
            // 
            this.picBoxAddContact.BackgroundImage = global::ChatMessengerGUI.Properties.Resources.iconplus;
            this.picBoxAddContact.Location = new System.Drawing.Point(429, 264);
            this.picBoxAddContact.Name = "picBoxAddContact";
            this.picBoxAddContact.Size = new System.Drawing.Size(39, 40);
            this.picBoxAddContact.TabIndex = 4;
            this.picBoxAddContact.TabStop = false;
            this.picBoxAddContact.Click += new System.EventHandler(this.picBoxAddContact_Click);
            // 
            // ucMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.picBoxAddContact);
            this.Controls.Add(this.lblContacts);
            this.Controls.Add(this.cmdConversation);
            this.Controls.Add(this.lblEmail);
            this.Name = "ucMain";
            this.Size = new System.Drawing.Size(760, 370);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxAddContact)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblEmail;
        private MetroFramework.Controls.MetroButton cmdConversation;
        private MetroFramework.Controls.MetroLabel lblContacts;
        private System.Windows.Forms.PictureBox picBoxAddContact;
    }
}

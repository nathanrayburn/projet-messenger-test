﻿using MetroFramework.Controls;
using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Net.Mail;
using Model;
using MySql.Data.MySqlClient;

namespace ChatMessengerGUI
{
    /// <summary>
    /// This user control is designed for the register page of our application and is inherited from an external windows form theme "Metro"
    /// </summary>
    public partial class ucRegister : MetroUserControl
    {

        #region private attributes
        private int indexLastChange;
        private StringBuilder password = new StringBuilder();
        private TimerCallback timerCallback;
        private System.Threading.Timer timer;

        // For the verification text box 
        private int indexLastChangeVerification;
        private StringBuilder passwordVerification = new StringBuilder();
        private TimerCallback timerCallbackVerification;
        private System.Threading.Timer timerVerification;
        #endregion private attributes

        #region constructor
        /// <summary>
        /// This constructor is used initially when navigating
        /// this initializes the components and instances a call back for timer
        /// </summary>
        public ucRegister()
        {
            InitializeComponent();
            timerCallback = new TimerCallback(Do);
            timerCallbackVerification = new TimerCallback(Do2);
        }
        #endregion constructor

        #region private methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void linkToLogin_Click(object sender, EventArgs e)
        {
            frmSign.Instance.Content.Controls.Clear();
            frmSign.Instance.Content.Controls.Add(new ucLogin());
        }

        private void cmdRegister_Click(object sender, EventArgs e)
        {
            bool noEmail = string.IsNullOrWhiteSpace(txtEmail.Text); 
            bool noPassword = string.IsNullOrWhiteSpace(txtPassword.Text);
            bool noPasswordVerification = string.IsNullOrWhiteSpace(txtPasswordVerification.Text);
            bool emailNotVaild = IsValid(txtEmail.Text);
            bool weakPassword = PasswordCheck.IsStrongPassword(password.ToString());

            if (noEmail || noPassword || noPasswordVerification)
            {
                MessageBox.Show(@"Il faut remplir tous les champs !", @"Erreur dans le formulaire", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if (noEmail) txtEmail.WithError = true;                                 //verify if no email
                if (noPassword) txtPassword.WithError = true;                           //verify if no password
                if (noPasswordVerification) txtPasswordVerification.WithError = true;   //verify if no password verification
                return;

            } else if (!emailNotVaild)
            {
                MessageBox.Show(@" Veuillez entre un e-mail valable !", @"Erreur dans le formulaire", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtEmail.WithError = true;
                return;

            } else if (!password.Equals(passwordVerification))
            {
                MessageBox.Show(@" Veuillez entrer deux fois le même mot de passe !", @"Erreur dans le formulaire", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPasswordVerification.WithError = true;
                txtPassword.WithError = true;
                return;

            } else if (!weakPassword)
            {
                MessageBox.Show(@" Votre mot de passe n'est pas assez fort !", @"Erreur dans le formulaire", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPassword.WithError = true;
                txtPasswordVerification.WithError = true;
                return;
            }

            User user = new User(txtEmail.Text, password.ToString());
            DBUserManager dbUserManager = new DBUserManager(Settings.Instance.DbConnectionString, user);
            try
            {
                dbUserManager.Register();
                frmSign.Instance.Content.Controls.Clear();
                frmSign.Instance.Content.Controls.Add(new ucValidate(txtEmail.Text));
            }
            catch (MySqlException exception)
            {
                MessageBox.Show("Erreur de connexion avec le serveur, ceci peut-être un problème technique.", @"Erreur | Chat Messenger");

            }
            catch (UserAlreadyExistsException)
            {
                MessageBox.Show("Erreur d'inscription !", @"Erreur | Chat Messenger");
            }
        }
        /// <summary>
        /// This function is designed to test if the email is valid or not
        /// </summary>
        /// <param name="emailaddress"></param>
        /// <returns></returns>
        private bool IsValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Destroys timer , and updates the last character to a star after 1 second
        /// </summary>
        /// <param name="state"></param>
        private void Do(object state)
        {
            int num = this.txtPassword.Text.Count(); //count the number of characters
            if (num > 0)
            {
                StringBuilder s = new StringBuilder(this.txtPassword.Text);

                s[num - 1] = '●';                                   //hide the last character after one second
                this.Invoke(new Action(() =>
                {
                    var cursorIndex = txtPassword.SelectionStart;   //get the cursor index
                    this.txtPassword.Text = s.ToString();           // update the textbox
                    this.txtPassword.SelectionStart = cursorIndex;
                    timer.Dispose();                                //destroy the timer
                    timer = null;
                }));
            }
        }

        /// <summary>
        /// Destroys timer , and updates the last character to a star after 1 second
        /// </summary>
        /// <param name="state"></param>
        private void Do2(object state)
        {
            int num = this.txtPasswordVerification.Text.Count();                     //count the number of characters
            if (num > 0)
            {
                StringBuilder s = new StringBuilder(this.txtPasswordVerification.Text);

                s[num - 1] = '●'; //hide the last character after one second
                this.Invoke(new Action(() =>
                {
                    var cursorIndex = txtPasswordVerification.SelectionStart;       //get the cursor index
                    this.txtPasswordVerification.Text = s.ToString();               // update the textbox
                    this.txtPasswordVerification.SelectionStart = cursorIndex;      //reset the cursor to the current cursor
                    timerVerification.Dispose();                                    //destroy the timer
                    timerVerification = null;
                }));
            }
        }

        /// <summary>
        ///  This function is designed to masking characters by characters with a period of 1 second
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPasswordVerification_TextChanged(object sender, EventArgs e)
        {
            StringBuilder input = new StringBuilder(txtPasswordVerification.Text); //set the input of the textbox to a string builder
            var cursorIndex = txtPasswordVerification.SelectionStart; // Get the starting position of the cursor
            //Go through the textbox's characters
            for (int i = 0; i < txtPasswordVerification.Text.Count(); i++) 
            {
                //Filter if there is a letter
                if (input[i] != '●')
                {
                    if (i >= passwordVerification.Length)
                    {
                        passwordVerification.Append(input[i]); 
                    }
                    else
                    {
                        if (input.Length > passwordVerification.Length)
                        {
                            passwordVerification.Insert(i, input[i]); //insert if a letter has been typed in the middle of the text into the clear stringBuilder
                        }
                        else
                        {
                            passwordVerification[i] = txtPasswordVerification.Text.ElementAt(i); //replace the letter
                        }
                    }

                    if (indexLastChangeVerification == i && input.Length > 0) //replace the clear character in the textbox
                    {
                        input[i] = '●';
                    }

                    indexLastChangeVerification = i; //Update the last index of a character that has changed
                }
            }

            int dif = passwordVerification.Length - input.Length;
            if (dif > 0)
            {
                passwordVerification.Remove(cursorIndex, dif); //remove a certain number of numbers in the clear password if have removed one or more characters
            }


            txtPasswordVerification.Text = input.ToString(); //update the textbox
            Console.WriteLine("password " + passwordVerification); //show the password in console

            if (timerVerification == null)
            {
                timerVerification = new System.Threading.Timer(timerCallbackVerification, null, 1000, 0); //create  a new timer if the old one has been destroyed
            }
            else
            {
                timerVerification.Change(1000, 0); //reset the timer to 1000ms
            }

            int num = this.txtPasswordVerification.Text.Count();
            if (num > 1)
            {
                StringBuilder s = new StringBuilder(this.txtPasswordVerification.Text);
                s[num - 2] = '●'; //replace the before to last character to a star
                this.txtPasswordVerification.Text = s.ToString();
                this.txtPasswordVerification.SelectionStart = num;
            }

            this.txtPasswordVerification.SelectionStart = (cursorIndex > 0) ? cursorIndex : 0; //third operator , test if the cursor is negative , it will go back to 0

            Console.WriteLine("Last index change :" + (indexLastChangeVerification)); //show the index in the console

        }

        /// <summary>
        ///  This function is designed to mask every character after 1 second
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            StringBuilder input = new StringBuilder(txtPassword.Text);
            var cursorIndex = txtPassword.SelectionStart;

            if (txtPassword.Text != "")
            {

                var Strength = PasswordCheck.GetPasswordStrength(password.ToString());
                lblStrength.Text = Strength.ToString(); //update the strength status

                switch (Strength.ToString()) //update the progression bar in function of the strength status
                {
                    case "Blank":
                        progressionBarStrength.Value = 20;
                        break;
                    case "VeryWeak":
                        progressionBarStrength.Value = 40;
                        break;
                    case "Weak":
                        progressionBarStrength.Value = 60;
                        break;
                    case "Medium":
                        progressionBarStrength.Value = 80;
                        break;
                    case "Strong":
                        progressionBarStrength.Value = 100;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                lblStrength.Text = "None";
                progressionBarStrength.Value = 0;
            }

            for (int i = 0; i < txtPassword.Text.Count(); i++) //Go through the textbox's characters
            {
                if (input[i] != '●')
                {
                    if (i >= password.Length)
                    {
                        password.Append(input[i]);
                    }
                    else
                    {
                        if (input.Length > password.Length)
                        {
                            password.Insert(i, input[i]); //insert if a letter has been typed in the middle of the text into the clear stringBuilder
                        }
                        else
                        {
                            password[i] = txtPassword.Text.ElementAt(i); //replace the letter
                        }
                    }

                    if (indexLastChange == i && input.Length > 0) //replace the clear character in the textbox
                    {
                        input[i] = '●';
                    }

                    indexLastChange = i; //Update the last index of a character that has changed
                }
            }

            int dif = password.Length - input.Length;
            if (dif > 0)
            {
                password.Remove(cursorIndex, dif); //remove a certain number of numbers in the clear password if have removed one or more characters

            }

            txtPassword.Text = input.ToString(); //update the textbox
            Console.WriteLine("password " + password); //show the password in console


            if (timer == null)
            {

                timer = new System.Threading.Timer(timerCallback, null, 1000, 0); //create  a new timer if the old one has been destroyed
            }
            else
            {
                timer.Change(1000, 0); //reset the timer to 1000ms
            }
            int num = this.txtPassword.Text.Count();
            if (num > 1)
            {
                StringBuilder s = new StringBuilder(this.txtPassword.Text);
                s[num - 2] = '●'; //replace the before to last character to a star
                this.txtPassword.Text = s.ToString();
                this.txtPassword.SelectionStart = num;
            }

            this.txtPassword.SelectionStart = (cursorIndex > 0) ? cursorIndex : 0; //third operator , test if the cursor is negative , it will go back to 0

            Console.WriteLine("Last index change :" + (indexLastChange)); //show the index in the console

        }

        #endregion  private methods
    }
}

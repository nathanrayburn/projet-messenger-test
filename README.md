# Chat Messenger

Projet "Chat Messenger" C# dans le cadre du CPNV.

## Utilsation en local

Pour pouvoir utiliser correctement l'application en local il faut :

- Lancer les script MySQL dans l'ordre
    1. _create_database.sql_
    2. _create_tables.sql_
    3. _create_user_admin.sql_ (dans le cas où l'utilisateur n'existe pas encore)
- Lancer l'installeur
- Executer le fichier depuis le .exe ou le raccourdi crée dans le bureau

## Dossiers

Le dossier _data_ contient le fichier de configuration Json et les scripts MySql qui sont dans le dossier _sql_

Les fichiers extraits par l'installeur sont placés dans le dossier "C:\Users\user\AppData\Roaming\YBZ_NRN\Chat Messenger"

---

**Have fun !**

/*
 * Author		: Yannick.BAUDRAZ@cpnv.ch
 * Version		: 02 DEC 2019
 * Description	: Create the database `test_ybznrn` for unit tests
 *                Make sur to run this script before run units tests
 */

/* Database */
DROP DATABASE IF EXISTS `test_ybznrn`;

CREATE DATABASE `test_ybznrn`;

USE `test_ybznrn`;

/* Users */
-- Create table
Create TABLE `users` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `email` VARCHAR(320) NOT NULL,
    `password` VARCHAR(60) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE (`email`)
) ENGINE = INNODB;

-- Insert some users
INSERT INTO
    users (email, password)
VALUES
    ('test@cpnv.ch', '$2a$11$IrDEvwmT1bG5bRyThWZlm.bhIivpvZYROGS9tTU9YEyNPTnKfxb2e'), -- qwe123!$
    ('yannick.baudraz@cpnv.ch', '$2a$11$ov08kIh6fWi.S6OFnAL0le119lEQl1ikuXruugcsul5sRX2nK1n.C'), -- Pa$$w0rd
    ('nathan.rayburn@cpnv.ch', '$2a$11$NWNbc29W.XJNfKh2y3SWeemfPvERLFo.GTtXNR2tDLDxZDed1E0qC'); -- W0rdPa$$

/* Database user */
DROP USER IF EXISTS 'test_ybznrn_tester' @'localhost';

CREATE USER 'test_ybznrn_tester' @'localhost' IDENTIFIED BY 'Pa$$w0rd';

GRANT USAGE ON *.* TO 'test_ybznrn_tester' @'localhost';

GRANT ALL ON `test_ybznrn`.* TO 'test_ybznrn_tester' @'localhost' WITH GRANT OPTION;

FLUSH PRIVILEGES;

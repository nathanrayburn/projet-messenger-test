/*
 * Author		: Yannick.BAUDRAZ@cpnv.ch
 * Version		: 02 DEC 2019
 * Description	: Create database `chat_messenger`
*/

DROP DATABASE IF EXISTS `chat_messenger`;
CREATE DATABASE `chat_messenger`;

/*
 * Author		: Yannick.BAUDRAZ@cpnv.ch
 * Version		: 02 DEC 2019
 * Description	: Create tables for database `chat_messenger`
*/

/* Database chat_messenger */
USE `chat_messenger`;

/* Drop tables */
DROP TABLE IF EXISTS `users`;

/* Table users */
Create TABLE `users` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(320) NOT NULL,
	`password` VARCHAR(60) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE (`email`)
);

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace TestChatMessenger
{
    /// <summary>
    /// This test class verify operations to <see cref="DBUserManager"/> class.
    /// </summary>
    /// <remarks>
    /// The database used is a test database and is created with the script `test_database.sql`.
    /// Make sure to run the sql script before each session, i.e. if there is a insert query and a unique constraint.
    /// </remarks>
    [TestClass]
    public class TestDBUserManager
    {
        private const string connectionString =
            "SERVER=localhost;DATABASE=test_ybznrn;UID=test_ybznrn_tester;PASSWORD=Pa$$w0rd";

        private DBUserManager userManager;
        private User user;

        /// <summary>
        /// This test method is designed to test when a user log in to the database.
        /// </summary>
        [TestMethod]
        public void Login_RightInformation_Success()
        {
            //given
            user = new User("test@cpnv.ch", "qwe123!$");
            userManager = new DBUserManager(connectionString, user);
            int expectedId = 1;

            //when
            userManager.Login();

            //then
            Assert.IsTrue(user.IsConnected);
            Assert.AreEqual(expectedId, user.Id);
        }

        /// <summary>
        /// This test method is designed to test when a user log in to the database.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(LoginFailException))]
        public void Login_EmailNotFound_Exception()
        {
            //given
            user = new User("test1234@cpnv.ch", "qwe123!$");
            userManager = new DBUserManager(connectionString, user);

            //when
            userManager.Login();
        }

        /// <summary>
        /// This test method is designed to test when a user log in to the database.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(LoginFailException))]
        public void Login_PasswordNotMatch_Exception()
        {
            //given
            user = new User("test@cpnv.ch", "notthepassword");
            userManager = new DBUserManager(connectionString, user);

            //when
            userManager.Login();
        }

        /// <summary>
        /// This test method is designed to test when a user try to register and log in with his new account.
        /// </summary>
        [TestMethod]
        public void Register_LoginAfter_Success()
        {
            //given
            user = new User("newUser@cpnv.ch", "Str0ng.Pa$$w0rd");
            userManager = new DBUserManager(connectionString, user);

            //when
            userManager.Register();
            userManager.Login();

            //then
            Assert.IsTrue(user.IsConnected);
        }

        /// <summary>
        /// This test method is designed to test when a user try to register with an email that already exists in the database.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(UserAlreadyExistsException))]
        public void Register_UserAlreadyExists_Exception()
        {
            //given
            user = new User("test@cpnv.ch", "Str0ng.Pa$$w0rd");
            userManager = new DBUserManager(connectionString, user);

            //when
            userManager.Register();
        }
    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace TestChatMessenger
{
    /// <summary>
    /// This test class is designed to verify operations on <see cref="PasswordCheck"/> class.
    /// </summary>
    [TestClass]
    public class TestPasswordCheck
    {
        private string password;
        private PasswordStrength expectedStrength;
        private PasswordStrength actualStrength;

        /// <summary>
        /// This method initialize variables before each test method.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            actualStrength = (PasswordStrength)6;
        }

        /// <summary>
        /// This test method is designed to test the strength of an empty password.
        /// </summary>
        [TestMethod]
        public void GetPasswordStrength_Empty_Blank()
        {
            //given
            password = "";
            expectedStrength = PasswordStrength.Blank;

            //when
            actualStrength = PasswordCheck.GetPasswordStrength(password);

            //then
            Assert.AreEqual(expectedStrength, actualStrength);
        }

        /// <summary>
        /// This test method is designed to test the strength of a password with only space chars.
        /// </summary>
        [TestMethod]
        public void GetPasswordStrength_SpaceChars_Blank()
        {
            //given
            password = "     ";
            expectedStrength = PasswordStrength.Blank;

            //when
            actualStrength = PasswordCheck.GetPasswordStrength(password);

            //then
            Assert.AreEqual(expectedStrength, actualStrength);
        }

        /// <summary>
        /// This test method is designed to test the strength of a password with less than 5 chars.
        /// </summary>
        [TestMethod]
        public void GetPasswordStrength_LessThan5Chars_VeryWeak()
        {
            //given
            password = "PAs$";
            expectedStrength = PasswordStrength.VeryWeak;

            //when
            actualStrength = PasswordCheck.GetPasswordStrength(password);

            //then
            Assert.AreEqual(expectedStrength, actualStrength);
        }

        /// <summary>
        /// This test method is designed to test the strength of a password between 5 and 7 digits chars.
        /// </summary>
        [TestMethod]
        public void GetPasswordStrength_Between5And7CharsDigits_VeryWeak()
        {
            //given
            password = "12345";
            expectedStrength = PasswordStrength.VeryWeak;

            //when
            actualStrength = PasswordCheck.GetPasswordStrength(password);

            //then
            Assert.AreEqual(expectedStrength, actualStrength);
        }

        /// <summary>
        /// This test method is designed to test the strength of a password between 5 and 7 LC chars.
        /// </summary>
        [TestMethod]
        public void GetPasswordStrength_Between5And7CharsLC_VeryWeak()
        {
            //given
            password = "passw";
            expectedStrength = PasswordStrength.VeryWeak;

            //when
            actualStrength = PasswordCheck.GetPasswordStrength(password);

            //then
            Assert.AreEqual(expectedStrength, actualStrength);
        }

        /// <summary>
        /// This test method is designed to test the strength of a password between 5 and 7 UC chars.
        /// </summary>
        [TestMethod]
        public void GetPasswordStrength_Between5And7CharsUC_VeryWeak()
        {
            //given
            password = "PASSW";
            expectedStrength = PasswordStrength.VeryWeak;

            //when
            actualStrength = PasswordCheck.GetPasswordStrength(password);

            //then
            Assert.AreEqual(expectedStrength, actualStrength);
        }

        /// <summary>
        /// This test method is designed to test the strength of a password with at least 8 LC and UC chars.
        /// </summary>
        [TestMethod]
        public void GetPasswordStrength_AtLeast8CharsLCAndUC_Weak()
        {
            //given
            password = "Password";
            expectedStrength = PasswordStrength.Weak;

            //when
            actualStrength = PasswordCheck.GetPasswordStrength(password);

            //then
            Assert.AreEqual(expectedStrength, actualStrength);
        }

        /// <summary>
        /// This test method is designed to test the strength of a password with at least 8 digit chars.
        /// </summary>
        [TestMethod]
        public void GetPasswordStrength_AtLeast8CharsDigit_Weak()
        {
            //given
            password = "12345678";
            expectedStrength = PasswordStrength.Weak;

            //when
            actualStrength = PasswordCheck.GetPasswordStrength(password);

            //then
            Assert.AreEqual(expectedStrength, actualStrength);
        }

        /// <summary>
        /// This test method is designed to test the strength of a password with at least 8 special chars.
        /// </summary>
        [TestMethod]
        public void GetPasswordStrength_AtLeast8CharsSpecial_Weak()
        {
            //given
            password = "-_-_-_-_";
            expectedStrength = PasswordStrength.Weak;

            //when
            actualStrength = PasswordCheck.GetPasswordStrength(password);

            //then
            Assert.AreEqual(expectedStrength, actualStrength);
        }

        /// <summary>
        /// This test method is designed to test the strength of a password with at least 8 LC and UC chars, and digit.
        /// </summary>
        [TestMethod]
        public void GetPasswordStrength_AtLeast8CharsLCAndUCAndDigits_Medium()
        {
            //given
            password = "Passw0rd";
            expectedStrength = PasswordStrength.Medium;

            //when
            actualStrength = PasswordCheck.GetPasswordStrength(password);

            //then
            Assert.AreEqual(expectedStrength, actualStrength);
        }

        /// <summary>
        /// This test method is designed to test the strength of a password with at least 8 LC, UC and special chars.
        /// </summary>
        [TestMethod]
        public void GetPasswordStrength_AtLeast8CharsLCAndUCAndSpecial_Medium()
        {
            //given
            password = "Pa$$word";
            expectedStrength = PasswordStrength.Medium;

            //when
            actualStrength = PasswordCheck.GetPasswordStrength(password);

            //then
            Assert.AreEqual(expectedStrength, actualStrength);
        }

        /// <summary>
        /// This test method is designed to test the strength of a password between 5 and 7 LC, UC and special chars, and digit.
        /// </summary>
        [TestMethod]
        public void GetPasswordStrength_Between5And7CharsLCAndUCDigitAndSpecial_Medium()
        {
            //given
            password = "P4$$w";
            expectedStrength = PasswordStrength.Medium;

            //when
            actualStrength = PasswordCheck.GetPasswordStrength(password);

            //then
            Assert.AreEqual(expectedStrength, actualStrength);
        }

        /// <summary>
        /// This test method is designed to test the strength of a password with at least 8 LC, UC and special chars, and digit.
        /// </summary>
        [TestMethod]
        public void GetPasswordStrength_AllStrongConditions_Strong()
        {
            //given
            password = "Pa$$w0rd"; // Pa$$w0rd is not a good password ! But is require the policy.
            expectedStrength = PasswordStrength.Strong;

            //when
            actualStrength = PasswordCheck.GetPasswordStrength(password);

            //then
            Assert.AreEqual(expectedStrength, actualStrength);
        }

        /// <summary>
        /// This test method is designed to verify if a password is strong or not.
        /// </summary>
        [TestMethod]
        public void IsStrongPassword_StrongPassword_Success()
        {
            //given
            password = "Pa$$w0rd";

            //when
            bool actualResult = PasswordCheck.IsStrongPassword(password);

            //then
            Assert.IsTrue(actualResult);
        }

        /// <summary>
        /// This test method is designed to verify if a password is strong or not.
        /// </summary>
        [TestMethod]
        public void IsStrongPassword_MediumPassword_Fail()
        {
            //given
            password = "Pa$$word";

            //when
            bool actualResult = PasswordCheck.IsStrongPassword(password);

            //then
            Assert.IsFalse(actualResult);
        }

        /// <summary>
        /// This method cleans all variable after each test method.
        /// </summary>
        [TestCleanup]
        public void CleanUp()
        {
            password = "";
            expectedStrength = 0;
            actualStrength = 0;
        }
    }
}
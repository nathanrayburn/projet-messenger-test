﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace TestChatMessenger
{
    /// <summary>
    /// This test class verify operations to <see cref="User"/> class.
    /// </summary>
    [TestClass]
    public class TestUser
    {
        /// <summary>
        /// This method test the properties of the user just after the instantiation.
        /// </summary>
        [TestMethod]
        public void NewUser_PropertiesAfterCreation_Success()
        {
            //given
            string email = "test@cpnv.ch";
            string password = "wordOfPass";
            int id = 17;
            bool connected = true;
            User user = new User(email, password) { Id = id, IsConnected = connected };

            //when
            string actualEmail = user.Email;
            string actualPassword = user.Password;
            int actualId = user.Id;
            bool actualConnectionState = user.IsConnected;

            //then
            Assert.AreEqual(email, actualEmail);
            Assert.AreEqual(password, actualPassword);
            Assert.AreEqual(id, actualId);
            Assert.AreEqual(connected, actualConnectionState);
        }
    }
}
﻿using System;
using MySql.Data.MySqlClient;

namespace Model
{
    /// <summary>
    /// This class manages the class <see cref="User"/> with the database.
    /// </summary>
    public class DBUserManager : DatabaseConnector
    {
        private readonly User user;

        /// <summary>
        /// This constructor instantiate the connection to the database and take the <see cref="User"/> to manage.
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="user"></param>
        public DBUserManager(string connectionString, User user) : base(connectionString)
        {
            this.connectionString = connectionString;
            this.user = user;
        }

        /// <summary>
        /// This method connect the user if his info match with the database.
        /// </summary>
        public void Login()
        {
            const string query = "SELECT id, email, password FROM users WHERE email = @email";
            MySqlParameter emailParam = new MySqlParameter("@email", user.Email);

            MySqlDataReader data = ExecuteQueryReader(query, emailParam);

            if (data.Read() && BCrypt.Net.BCrypt.Verify(user.Password, data["password"].ToString()))
            {
                user.IsConnected = true;
                user.Id = int.Parse(data["id"].ToString());
            }
            else if (!data.Read() || BCrypt.Net.BCrypt.Verify(user.Password, data["password"].ToString()))
            {
                throw new LoginFailException();
            }

            CloseConnection();
        }

        /// <summary>
        /// This method register the user.
        /// </summary>
        public void Register()
        {
            string hashedPassword = BCrypt.Net.BCrypt.HashPassword(user.Password);

            const string query = "INSERT INTO users (email, password) VALUES (@email, @password)";
            MySqlParameter emailParam = new MySqlParameter("@email", user.Email);
            MySqlParameter passwordParam = new MySqlParameter("@password", hashedPassword);

            try
            {
                ExecuteQuery(query, emailParam, passwordParam);
            }
            catch (MySqlException exception)
            {
                if (exception.Number == 1062)
                    throw new UserAlreadyExistsException();

                throw;
            }
        }
    }

    /// <summary>
    /// Exception when the login fail because the email isn't found or the password doesn't match.
    /// </summary>
    public class LoginFailException : Exception
    {
    }

    /// <summary>
    /// Exception when a user is already in the database.
    /// </summary>
    public class UserAlreadyExistsException : Exception
    {
    }
}
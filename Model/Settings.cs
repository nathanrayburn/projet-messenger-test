﻿using Newtonsoft.Json;

namespace Model
{
    /// <summary>
    /// The user's settings for the runtime application.
    /// </summary>
    [JsonObject]
    public class Settings
    {
        #region private attributes

        private static Settings instance;
        private int windowsY = -1;
        private int windowsX = -1;
        private string dbConnectionString = "";
        private string language = "";
        private string userEmail = "";

        #endregion

        #region private constructor

        private Settings()
        {
        }

        #endregion

        #region accessors

        /// <summary>
        /// The single instance of the class.
        /// </summary>
        public static Settings Instance => instance ?? (instance = new Settings());

        /// <summary>
        /// The location Y of the left-top corner of the windows.
        /// </summary>
        public int WindowsY
        {
            get => windowsY;
            set => windowsY = value;
        }

        /// <summary>
        /// The location X of the left-top corner of the windows.
        /// </summary>
        public int WindowsX
        {
            get => windowsX;
            set => windowsX = value;
        }

        /// <summary>
        /// The string to connect to the database.
        /// </summary>
        public string DbConnectionString
        {
            get => dbConnectionString;
            set => dbConnectionString = value;
        }

        /// <summary>
        /// Language of the application.
        /// </summary>
        public string Language
        {
            get => language;
            set => language = value;
        }

        /// <summary>
        /// Email of the last user connected if he checked "Remember me" button.
        /// </summary>
        public string UserEmail
        {
            get => userEmail;
            set => userEmail = value;
        }

        #endregion

        #region public methods

        /// <summary>
        /// This methods transforms a JSON string to the class <see cref="Settings"/> herself.
        /// </summary>
        /// <param name="jsonString">The JSON string to transform into the Settings class.</param>
        /// <remarks>JSON fields need to be exactly like the accessors name.</remarks>
        public Settings FromJson(string jsonString) => instance = JsonConvert.DeserializeObject<Settings>(jsonString);

        /// <summary>
        /// This method transforms the class <see cref="Settings"/> to a JSON string.
        /// </summary>
        /// <returns>A JSON string representation of the object.</returns>
        public string ToJson() => JsonConvert.SerializeObject(this);

        #endregion
    }
}
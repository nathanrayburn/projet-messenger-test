﻿namespace Model
{
    /// <summary>
    /// This class represent an user.
    /// </summary>
    public class User
    {
        #region private attributes

        private int id;
        private string email;
        private string password;
        private bool isConnected;

        #endregion

        #region constructors

        /// <summary>
        /// This constructor instantiate an user with an email and a password.
        /// </summary>
        /// <param name="email">The unique email</param>
        /// <param name="password">The password.</param>
        public User(string email, string password)
        {
            this.email = email;
            this.password = password;
        }

        #endregion constructors

        #region accessors

        /// <summary>
        /// Unique identifier.
        /// </summary>
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Unique email of the user.
        /// </summary>
        public string Email
        {
            get { return email; }
        }

        /// <summary>
        /// Password of the user.
        /// </summary>
        /// <remarks>The password is hashed.</remarks>
        public string Password
        {
            get { return password; }
        }

        /// <summary>
        /// If the user is connected to the application or not.
        /// </summary>
        public bool IsConnected
        {
            get { return isConnected; }
            set { isConnected = value; }
        }

        #endregion accessors
    }
}
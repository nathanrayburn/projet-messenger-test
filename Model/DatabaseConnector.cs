﻿using System.Data;
using MySql.Data.MySqlClient;

namespace Model
{
    /// <summary>
    /// This class is a generic connector to the database.
    /// </summary>
    public abstract class DatabaseConnector
    {
        #region Attributes

        #region Protected attributes

        /// <summary>
        /// A string that contains properties used to open the database. 
        /// </summary>
        protected string connectionString;

        #endregion

        #region Private attributes

        private MySqlConnection connection;

        #endregion

        #endregion

        #region constructors

        /// <summary>
        /// This constructor instantiate the database connection with the connection string.
        /// </summary>
        /// <param name="connectionString">The string that allow the connection to the database.</param>
        protected DatabaseConnector(string connectionString)
        {
            this.connectionString = connectionString;
            connection = new MySqlConnection();
        }

        #endregion

        #region Protected methods

        /// <summary>
        /// This methods execute a query that doesn't returns any row.<para/>
        /// Insert, Update, Delete...
        /// </summary>
        /// <param name="query">The MySQL query string to execute.</param>
        /// <param name="parameters">Optional parameters to a MySqlCommand.</param>
        protected void ExecuteQuery(string query, params MySqlParameter[] parameters)
        {
            OpenConnection();

            MySqlCommand command = new MySqlCommand(query, connection);
            foreach (MySqlParameter param in parameters) command.Parameters.Add(param);
            command.Prepare();
            command.ExecuteNonQuery();
            command.Dispose();

            CloseConnection();
        }

        /// <summary>
        /// This methods execute a query that return normally a row.
        /// </summary>
        /// <param name="query">The MySQL query string to execute.</param>
        /// <param name="parameters">Optional parameters to a MySqlCommand.</param>
        /// <returns>The result of the query.</returns>
        /// <remarks>Do not forget to dispose the returned object and close the connection when finish implementation.</remarks>
        protected MySqlDataReader ExecuteQueryReader(string query, params MySqlParameter[] parameters)
        {
            OpenConnection();

            MySqlCommand command = new MySqlCommand(query, connection);
            foreach (MySqlParameter param in parameters) command.Parameters.Add(param);
            command.Prepare();
            return command.ExecuteReader();
        }

        /// <summary>
        /// Open the connection to the database and keep it in the memory.
        /// </summary>
        protected void OpenConnection()
        {
            if (connection.State != ConnectionState.Closed) return;
            connection = new MySqlConnection(connectionString);
            connection.Open();
        }

        /// <summary>
        /// Close the connection to the database and clean the memory.
        /// </summary>
        protected void CloseConnection()
        {
            if (connection.State == ConnectionState.Closed) return;
            connection.Close();
        }

        #endregion
    }
}
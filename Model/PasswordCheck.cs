﻿using System.Linq;
using static System.String;

namespace Model
{
    /// <summary>
    /// Enum to define the password strength.
    /// Author : https://bit.ly/2Y3VUKW
    /// </summary>
    public enum PasswordStrength
    {
        /// <summary>
        /// Blank Password (empty and/or space chars only)
        /// </summary>
        Blank = 0,

        /// <summary>
        /// Either too short (less than 5 chars), one-case letters only or digits only
        /// </summary>
        VeryWeak = 1,

        /// <summary>
        /// Two strong condition met (>= 8 chars, UC letters and LC letters, digits or special chars)
        /// </summary>
        Weak = 2,

        /// <summary>
        /// Three strong conditions met (>= 8 chars, UC letters and LC letters, digits or special chars)
        /// </summary>
        Medium = 3,

        /// <summary>
        /// All strong conditions met (>= 8 chars, UC letters and LC letters, digits or special chars)
        /// </summary>
        Strong = 4
    }

    /// <summary>
    /// Utility class to check the strength of a password.
    /// Author : https://bit.ly/2Y3VUKW
    /// Update : Yannick Baudraz
    /// Version : 30-NOV-2019
    /// </summary>
    public static class PasswordCheck
    {
        /// <summary>
        /// This method retrieves the strength of a password.
        /// </summary>
        /// <param name="password">The password to check</param>
        /// <returns>The strength of the password.</returns>
        /// <remarks>Use this for general purpose scenarios, i.e. when you don't have a strict policy to follow.</remarks>
        public static PasswordStrength GetPasswordStrength(string password)
        {
            int score = 0;

            if (IsNullOrEmpty(password) || IsNullOrEmpty(password.Trim())) return PasswordStrength.Blank;
            if (!HasMinimumLength(password, 5)) return PasswordStrength.VeryWeak;
            if (HasOnlyUpperCaseLetter(password) && !HasMinimumLength(password, 8)) return PasswordStrength.VeryWeak;
            if (HasOnlyLowerCaseLetter(password) && !HasMinimumLength(password, 8)) return PasswordStrength.VeryWeak;
            if (HasOnlyDigit(password) && !HasMinimumLength(password, 8)) return PasswordStrength.VeryWeak;
            if (HasMinimumLength(password, 8)) score++;
            if (HasUpperCaseLetter(password) && HasLowerCaseLetter(password)) score++;
            if (HasDigit(password)) score++;
            if (HasSpecialChar(password)) score++;

            return (PasswordStrength)score;
        }

        /// <summary>
        /// This method is designed to tell if a password is strong or not.
        /// </summary>
        /// <remarks>
        /// Sample password policy implementation :<para/>
        /// - minimum 8 characters<para/>
        /// - at least one UC letter and one LC letter<para/>
        /// - at least one digit<para/>
        /// - at least one special char<para/>
        /// </remarks>
        /// <param name="password">The password to check.</param>
        /// <returns>TRUE if the password is strong enough.</returns>
        public static bool IsStrongPassword(string password)
        {
            return GetPasswordStrength(password) == PasswordStrength.Strong;
        }

        #region Private helper methods

        private static bool HasMinimumLength(string password, int minLength)
        {
            return password.Length >= minLength;
        }

        private static bool HasDigit(string password)
        {
            return password.Any(char.IsDigit);
        }

        private static bool HasSpecialChar(string password)
        {
            return password.IndexOfAny("!@#$%^&*?_~-£().,".ToCharArray()) != -1;
        }

        private static bool HasUpperCaseLetter(string password)
        {
            return password.Any(char.IsUpper);
        }

        private static bool HasLowerCaseLetter(string password)
        {
            return password.Any(char.IsLower);
        }

        private static bool HasOnlyUpperCaseLetter(string password)
        {
            return password.All(char.IsUpper);
        }

        private static bool HasOnlyLowerCaseLetter(string password)
        {
            return password.All(char.IsLower);
        }

        private static bool HasOnlyDigit(string password)
        {
            return password.All(char.IsDigit);
        }

        #endregion
    }
}